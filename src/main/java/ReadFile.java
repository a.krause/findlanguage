import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {
    public  String text (String title) throws URISyntaxException{
        URI fileUri = getClass().getClassLoader().getResource(title).toURI();
        Path filePath =  Paths.get(fileUri);

        Charset charset =Charset.forName("UTF-8");

        String lane=" ";
        lane=getWholeText(filePath, charset, lane);
        return lane;
    }

    private String getWholeText(Path filePath, Charset charset, String lane) {
        try (BufferedReader bufferedReader = Files.newBufferedReader((java.nio.file.Path) filePath, charset)){
            lane=bufferedReader.readLine();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return lane;
    }
    public List <String> getFiles(){
        List <String> files= new ArrayList<>();
        File[] file = new File("src/main/resources").listFiles();
        for (File f:file)
        if (f.isFile()){
            files.add(f.getName());
            
        }

        return files;
    }
}
//a.krause